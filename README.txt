CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Disable Auto-populate Registration Form From Browser intends to restrict/prevent
auto-population on the registration form.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/disable_autopopulate_rform


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Ravi Kumar Singh (rksyravi) - https://www.drupal.org/u/rksyravi
